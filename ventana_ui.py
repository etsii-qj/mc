# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'VentanaPrincipal.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(842, 577)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.button_01 = QtWidgets.QPushButton(self.centralwidget)
        self.button_01.setGeometry(QtCore.QRect(590, 20, 151, 21))
        self.button_01.setObjectName("button_01")
        self.label_descripcion_1 = QtWidgets.QLabel(self.centralwidget)
        self.label_descripcion_1.setGeometry(QtCore.QRect(40, 20, 251, 71))
        self.label_descripcion_1.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.label_descripcion_1.setObjectName("label_descripcion_1")
        self.button_lyapunov = QtWidgets.QPushButton(self.centralwidget)
        self.button_lyapunov.setGeometry(QtCore.QRect(590, 50, 151, 21))
        self.button_lyapunov.setObjectName("button_lyapunov")
        self.button_feigenbaum = QtWidgets.QPushButton(self.centralwidget)
        self.button_feigenbaum.setGeometry(QtCore.QRect(170, 520, 131, 21))
        self.button_feigenbaum.setObjectName("button_feigenbaum")
        self.button_map = QtWidgets.QPushButton(self.centralwidget)
        self.button_map.setGeometry(QtCore.QRect(550, 520, 131, 21))
        self.button_map.setObjectName("button_map")
        self.line_funcion = QtWidgets.QLineEdit(self.centralwidget)
        self.line_funcion.setGeometry(QtCore.QRect(310, 20, 171, 21))
        self.line_funcion.setObjectName("line_funcion")
        self.label_resultado = QtWidgets.QLabel(self.centralwidget)
        self.label_resultado.setGeometry(QtCore.QRect(100, 100, 611, 31))
        self.label_resultado.setObjectName("label_resultado")
        self.label_grafica = QtWidgets.QLabel(self.centralwidget)
        self.label_grafica.setGeometry(QtCore.QRect(110, 150, 701, 321))
        self.label_grafica.setText("")
        self.label_grafica.setObjectName("label_grafica")
        self.slider_r = QtWidgets.QSlider(self.centralwidget)
        self.slider_r.setGeometry(QtCore.QRect(310, 50, 171, 22))
        self.slider_r.setMaximum(40)
        self.slider_r.setSingleStep(1)
        self.slider_r.setOrientation(QtCore.Qt.Horizontal)
        self.slider_r.setObjectName("slider_r")
        self.label_r = QtWidgets.QLabel(self.centralwidget)
        self.label_r.setGeometry(QtCore.QRect(490, 50, 51, 16))
        self.label_r.setText("")
        self.label_r.setObjectName("label_r")
        self.label_descripcion_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_descripcion_2.setGeometry(QtCore.QRect(370, 70, 61, 16))
        self.label_descripcion_2.setObjectName("label_descripcion_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 842, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Tests para la detección del caos"))
        self.button_01.setText(_translate("MainWindow", "Test 0-1"))
        self.label_descripcion_1.setText(_translate("MainWindow", "Introduzca la funcion\n"
"(se permite el uso de símbolos \'a\' y \'x\' para definirla):\""))
        self.button_lyapunov.setText(_translate("MainWindow", "Exponente de Lyapunov"))
        self.button_feigenbaum.setText(_translate("MainWindow", "Diagrama de Feigenbaum"))
        self.button_map.setText(_translate("MainWindow", "Representación gráfica"))
        self.label_resultado.setText(_translate("MainWindow", "Resultado del Test: "))
        self.label_descripcion_2.setText(_translate("MainWindow", "Valor de r"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

