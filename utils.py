import math
import sympy as sp
from sympy.parsing.sympy_parser import parse_expr
import numpy as np
import matplotlib.pyplot as plt

def logistica(r, x):
    return r * x * (1 - x)

def lyapunov_logistica(r, iteraciones=1000, x0=1e-5):
    x = x0
    res = 0
    for i in range(iteraciones):
        x = logistica(r, x)
        res += np.log(abs(r - 2 * r * x))
    return res/iteraciones

def lyapunov_logistica_plot(r_to_plot,n=1000, xmin=0, xmax=4, iteraciones=1000, x0=1e-5):
    x = x0 * np.ones(n)
    r = np.linspace(xmin, xmax, n)
    res = np.zeros(n)

    plt.clf()
    fig, axis = plt.subplots(1, 1, figsize=(6, 3))
    for i in range(iteraciones):
        x = logistica(r, x)
        res += np.log(abs(r - 2 * r * x))

    axis.axhline(0, color='k', lw=.5, alpha=.5)
    axis.axvline(r_to_plot, color='red', lw=.5, alpha=.5)

    axis.plot(r,res/iteraciones,'-k', alpha=0.25)
    axis.plot(r[res < 0],
             res[res < 0] / iteraciones,
             '.b', alpha=1, ms=1)

    axis.plot(r[res >= 0],
             res[res >= 0] / iteraciones,
             '.r', alpha=1, ms=1)
    axis.set_xlim(xmin, xmax)
    axis.set_xlabel("a")
    axis.set_ylim(-2, 1)
    axis.set_ylabel("exponente de lyapunov")
    axis.set_title("Exponente de Lyapunov en función de r")
    plt.tight_layout()
    plt.savefig('foo.png')

def lyapunov(r, x0=0.7, iter=500, f=None):
    var_x = sp.Symbol('x')
    var_r = sp.Symbol('r')
    fun = parse_expr(f)
    df = sp.diff(f, var_x)
    s = 0
    x = x0
    for i in range(200):
        x = fun.subs({var_x: x, var_r: r})
    for i in range(iter):
        val = np.float64(abs(df.subs({var_x: x, var_r: r})))
        s = s + np.log(val)
        x = fun.subs({var_x: x, var_r: r})
    return s / iter


def lyapunov_plot(r_to_plot,n=80, xmin=0, xmax=4, iteraciones=300, x0=0.8,fun='a*x*(1-x)'):
    r = np.linspace(xmin, xmax, n)
    res = np.zeros(n)
    plt.clf()
    fig, axis = plt.subplots(1, 1, figsize=(6, 3))
    for i,e in enumerate(r):
        res[i] = lyapunov(e,x0,iteraciones,fun)

    axis.axhline(0, color='k', lw=.5, alpha=.5)
    axis.axvline(r_to_plot, color='red', lw=.5, alpha=.5)

    axis.plot(r,res,'k', alpha=0.25)
    axis.plot(r[res < 0],
             res[res < 0],
             '.b', alpha=1, ms=3)

    axis.plot(r[res >= 0],
             res[res >= 0],
             '.r', alpha=1, ms=3)
    axis.set_xlim(xmin, xmax)
    axis.set_xlabel("a")
    axis.set_ylim(-2, 1)
    axis.set_ylabel("exponente de lyapunov")
    axis.set_title("Exponente de Lyapunov")
    plt.tight_layout()
    plt.savefig('foo.png')


def bifurcacion_logistica(n=1000, xmin=0, xmax=4, iteraciones=1000, n_bifurcaciones=100, x0=1e-5):
    x = x0 * np.ones(n)
    r = np.linspace(xmin, xmax, n)
    res = np.zeros(n)

    plt.clf()
    fig, axis = plt.subplots(1, 1, figsize=(6, 3))
    for i in range(iteraciones):
        x = logistica(r, x)
        res += np.log(abs(r - 2 * r * x))
        if i >= (iteraciones - n_bifurcaciones):
            axis.plot(r, x, ',k', alpha=.25)
    axis.set_xlim(xmin, xmax)
    axis.set_xlabel("r")
    axis.set_ylabel("x")
    axis.set_title("Diagrama de bifurcación")
    plt.tight_layout()
    plt.savefig('foo.png')



def searchK(r, n=100000, N=5000, x0=0.9, c=0.7, f=None):
    def pq(phi, c_aux, n, N):
        p = np.zeros(n+N)
        q = np.zeros(n+N)

        p[0] = phi[0] * np.cos(c_aux)
        q[0] = phi[0] * np.sin(c_aux)

        for i in range(n+N-1):
            p[i+1] = p[i] + phi[i] * math.cos(i * c_aux)
            q[i+1] = q[i] + phi[i] * math.sin(i * c_aux)
        return p, q

    def phiscript(f, r, n, N, x0):
        var_x = sp.Symbol('x')
        var_r = sp.Symbol('r')
        if f:
            f = parse_expr(f)
        res = np.zeros(n+N)
        x = x0
        for i in range(200):
            if f:
                x = f.subs({var_x: x, var_r: r})
            else:
                x = logistica(r,x)
        for i in range(n+N):
            if f:
                x = f.subs({var_x: x, var_r: r})
            else:
                x = logistica(r,x)
            res[i] = x
        return res

    s = 0
    phi = phiscript(f, r, n, N, x0)
    p, q = pq(phi, c, n, N)

    for i in range(N):
        s = s + ((p[i+n]-p[i])**2 + (q[i+n]-q[i])**2)

    M = s/N
    K = np.log(M)/np.log(n)

    plt.clf()
    fig, axis = plt.subplots(1, 1, figsize=(6, 3))

    axis.plot(q, p ,'b', alpha=1, ms=3)
    axis.set_xlim(min(q)-1, max(q)+1)
    axis.set_xlabel("p")
    axis.set_ylim(min(p)-1, max(p)+1)
    axis.set_ylabel("q")
    axis.set_title("Coeficiente K")
    plt.tight_layout()
    plt.savefig('foo.png')

    return K

