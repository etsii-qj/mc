Para poder usar la aplicación el primer paso necesario será descargarse los archivos con el código fuente. 

En segundo lugar será necesario abrir una ventana de comandos y situarse en el directorio donde esté todo descargado, aquí se deberá ejecutar el siguiente código:

`pip install -r requirements.txt` ó `pip3 install -r requirements.txt` en caso de usar python2 como versión principal de python para el equipo


Para abrir la aplicación será necesario ejecutar python ventana_principal.py mediante:

`python app.py` ó `python3 app.py` en caso de usar python3